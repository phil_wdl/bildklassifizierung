FROM python:3.8-slim-buster
RUN apt update
RUN mkdir -p /app/
COPY anforderungen_pip.txt anforderungen_sudo.txt bk.ipynb /app/
RUN pip install -r /app/anforderungen_pip.txt
RUN pip install jupyter
RUN apt-get update && apt-get install -y python3-tk #apt install /app/anforderungen_sudo.txt
WORKDIR /app/
CMD ["jupyter", "notebook", "--notebook-dir=/app/", "--ip=0.0.0.0", "--no-browser", "--allow-root"] 

#CMD ["uvicorn", "bk:app", "--host", "0.0.0.0"]

