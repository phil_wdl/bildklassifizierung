import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt

modeliste = keras.datasets.fashion_mnist;

(train_images, train_labels), (test_images, test_labels) = modeliste.load_data()

class_names = ['T-shirt/top', 'Sneaker', 'Pullover', 'Ankle boot', 'Coat', 'Sandal', 'Shirt', 'Trouser', 'Bag', 'Dress']

train_images.shape 

plt.figure()
plt.imshow(train_images[0])
plt.colorbar()
plt.grid(False)
plt.show()

train_images = train_images / 255.0
test_images = test_images / 255.0

plt.figure(figsize=(10,10))
for i in range(25):
    plt.subplot(5,5,i+1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(train_images[i], cmap=plt.cm.binary)
    plt.xlabel(class_names[train_labels[i]])
plt.show()

# setup the layer
model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(128, activation='relu'),
    keras.layers.Dense(10)
])

#Fitting the Model
model.fit(train_images, train_labels, epochs=10)
#Evaluating Accuracy
test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)
print('\nTest accuracy:', test_acc)

#Fitting the Model
model.fit(train_images, train_labels, epochs=10)
#Evaluating Accuracy
test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)
print('\nTest accuracy:', test_acc)




"""



import tensorflow as tf
from tensorflow import keras

# Laden Sie das Fashion MNIST Datensatz
(x_train, y_train), (x_test, y_test) = keras.datasets.fashion_mnist.load_data()

# Normalisieren Sie die Daten
x_train = x_train / 255.0
x_test = x_test / 255.0

# Erstellen Sie das Modell
model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(128, activation='relu'),
    keras.layers.Dense(10, activation='softmax')
])

# Kompilieren Sie das Modell
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

# Trainieren Sie das Modell
model.fit(x_train, y_train, epochs=10)

# Testen Sie das Modell
test_loss, test_acc = model.evaluate(x_test, y_test, verbose=2)
print('\nTest accuracy:', test_acc)

# Verwenden Sie das Modell, um ein Bild zu klassifizieren
predictions = model.predict(x_test)"""